#
# Be sure to run `pod lib lint GZCTimerHelper.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'GZCTimerHelper'
  s.version          = '0.1.3'
  s.summary          = '统一的计时器管理帮助类'

  s.description      = <<-DESC
TODO: 统一的计时器管理帮助类，实现代理直接添加监听即可实现计时
                       DESC

  s.homepage         = 'https://gitlab.com/GZCFrameworks_swift/GZCTimerHelper'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Guo ZhongCheng' => 'gzhongcheng@qq.com' }
  s.source           = { :git => 'https://gitlab.com/GZCFrameworks_swift/GZCTimerHelper.git', :tag => s.version.to_s }
  
  
  s.ios.deployment_target = '9.0'
  s.swift_version = "5.1"

  s.source_files = 'GZCTimerHelper/Classes/**/*'
  s.xcconfig = { 'DEFINES_MODULE' => "YES" }
end
