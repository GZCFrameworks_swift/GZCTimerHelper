//
//  ViewController.swift
//  GZCTimerHelper
//
//  Created by Guo ZhongCheng on 10/26/2020.
//  Copyright (c) 2020 Guo ZhongCheng. All rights reserved.
//

import UIKit
import GZCTimerHelper

class ViewController: UIViewController {
    
    let keepModel = TestModel("其他")
    let shareModel = TestModel("单例")
    
    var keepModels = [TestModel]()
    
    let countDownModel = CountDownModel()
    var countDownObserver: NSKeyValueObservation!
    @IBOutlet weak var countDownLabel: UILabel!
    @IBOutlet weak var countDownButton: UIButton!
    
    // 创建不同间隔的临时计时器（尽量少用）
    let otherTimer: GZCTimerHelper = GZCTimerHelper(3)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        otherTimer.addObserver(keepModel)
        
        // 使用单例计时器
        GZCTimerHelper.shared.addObserver(shareModel)
        
        /// 马上被释放的对象，Helper中的引用都是weak指针的，因此不需要考虑释放的问题
        let tempModel = TestModel("马上释放的对象")
        GZCTimerHelper.shared.addObserver(tempModel)
        
        /// 模拟大量注册数据
//        for i in 0..<30 {
//            let tempModel = TestModel("\(i)")
//            GZCTimerHelper.shared.addObserver(tempModel)
//            keepModels.append(tempModel)
//        }
        
        countDownModel.onChanged = { [weak self] newValue in
            DispatchQueue.main.async {
                self?.countDownLabel.text = newValue
            }
        }
    }

    
    @IBAction func beginCountDown(_ sender: Any) {
        countDownModel.endTime = Date().timeIntervalSince1970 + 10
        GZCTimerHelper.shared.addObserver(countDownModel)
    }
}

class TestModel: NSObject, GZCTimerHelperDelegate {
    var name: String?
    
    init(_ name: String) {
        self.name = name
    }
    
    func timerImplement() {
        // 模拟耗时操作
        Thread.sleep(forTimeInterval: 0.1)
        print(name ?? "")
    }
}

class CountDownModel: GZCTimerHelperDelegate {
    
    var endTime: TimeInterval = 0
    var onChanged: ((_ newValue: String)->Void)?
    
    func timerImplement() {
        let currentTime = Date().timeIntervalSince1970
        if currentTime < endTime {
            onChanged?("剩余：\(Int(endTime - currentTime))秒")
        } else {
            GZCTimerHelper.shared.removeObserver(self)
            onChanged?("倒计时结束，已移除")
            // 模拟耗时操作
            Thread.sleep(forTimeInterval: 0.3)
            print("耗时操作结束")
        }
    }
}
